# A SHORT NOTES ON BROWSER RENDERING


## BROWSER RENDERING

#### Defination :

Browser Rendering is the process to change HTML, CSS, JavaScript code into an interactive page.When user perform actions on page it changes.

## HTML :

- HTML stands for Hyper Text Markup Language.
- HTML is a standard markup language for creating web pages.
- HTML tells the browser how to display the content.

## CSS :

- CSS stands for Cascading Style Sheets.
- CSS is used to style the content of web page.

## JAVASCRIPT :

- JavaScript is a scripting language used to develop web pages.
- We can use JavaScript in both frontend and backend.

## DOM :

- DOM stands for Document Object Model.
- By using DOM in JavaScript we can manipulate the content of web pages.

DOM Tree:

![alt text](https://www.freecodecamp.org/news/content/images/2021/09/Document.jpg "optional title")




# MECHANISM OF BROWSER RENDERING 

To render HTML, CSS, JavaScript to DOM involves some steps.

Bowser Rendering Tree:

![alt text](https://miro.medium.com/v2/resize:fit:1400/1*3Imspi9H9gdXg92ofdR7wQ.png "optional title")





Rendering steps follows:

### 1. HTML Parsing:

- The browser downloads the HTML file and begins parsing it from top to bottom.
- After that it converts tags to tree structure called DOM tree.

### 2. CSS Parsing:

- Along with HTML parsing the browser parses CSS styles.
- After that it converts all CSS styles to tree structure called CSSOM (CSS Object Model).

### 3. Constructing Render Tree:

- The browser combines both DOM and CSSOM to creat Render Tree.
- Each node in the Render Tree contains the visual information required for rendering, including style information.

### 4. Layout:

- The browser calculates the geometric positions and sizes of all the elements in the Render Tree.
- This step is called layout or reflow.


### 5. Painting:

- The browser than converts Render Tree into actual pictures on the screen.
- This involves drawing the visual representation of elements like test, colour, images, etc., .
- This process can be complex and might be optimized by the browser for performance.

### 6. JavaScript Execution:

- As the browser parses HTML and CSS, it may encounter script tags.
- By using DOM JavaScript changes or manipulates the HTML and CSS content.

Code Samples:

```html
HTML

<body>
    <div>
    <p class="p" id="##">Hello World!</p>
    </div>
    <script src="script.js"></script>       
</body>
```

```css
CSS

.p{
    color: blue;
}
```

```js
JavaScript

let para=document.getElementById("##");
para.textContent="Hello Sai";

```

### References Links

[https://www.youtube.com/watch?v=n1cKlKM3jYI&t=1s]

[https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work]



