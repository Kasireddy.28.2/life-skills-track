# Grit and Growth Mindset

## Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
The video is about that we have strong passion and detremination to achive success in life.
## Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
The video explains the growth mindset, highlighting that learning abilities are not static but can be enhanced through effort. It discusses how adopting a growth mindset can lead to better learning outcomes and greater success.

## What is the Internal Locus of Control? What is the key point in the video?
The internal locus of control means believing that you have control over the outcomes in your life. The video likely explains that adopting this belief can boost your motivation and help you achieve your goals because you feel responsible for your own success.

## What are the key points mentioned by speaker to build growth mindset (explanation not needed).
The video likely discusses methods and tips for developing a growth mindset, but exact details are not mentioned here.

## What are your ideas to take action and build Growth Mindset?

- View challenges as opportunities to learn and grow.
- Accept constructive criticism and use it to improve.
- Analyze what went wrong and how to do better next time.
- Stay open to new experiences and learning opportunities.
- Engage with people who have a growth mindset and who support your development.
- Replace negative thoughts with positive, growth-oriented affirmations.
- Teaching others can reinforce your own growth mindset principles.
- Focus on what you have learned and the progress you've made, rather than what you lack.