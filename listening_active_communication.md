# Listening and Active Communication

## What are the steps/strategies to do Active Listening?

- Pay attention to the speaker.
- Show to the speaker that you are listening by body language.
- Before we ask allow the speaker to finish his points.
- Ask doubts.
- Make a note on important points.
- Respond appropriately.

## According to Fisher's model, what are the key points of Reflective Listening?

- Understanding before Asking.
- Pay attention to speaker.
- Show that you are listening through your body language.
- Show that you are listening through your words.

## What are the obstacles in your listening process?

- Not interested in listening when my mind is not present.
- Distractions from outside.

## What can you do to improve your listening?

- I have to be open minded when I am listening and I don't think other things when I am listening.
- I don't think about outside distractions and focus on listening.

## When do you switch to Passive communication style in your day to day life?

- When I feel low confident.
- When I'm tired.

## When do you switch into Aggressive communication styles in your day to day life?

- Past experiences that happen in present.
- When I get anger and frustration about things are person.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- When I don't want direct conflict but I need to exprees my disagrement.
- If I feel that my efforts are not recognized.

## How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

- Maintaining eye contact.
- Learn to say no some things.
- I have prepare more for difficult conversations.
- I have to boost my self-confidence.
