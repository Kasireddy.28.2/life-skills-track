# Good Practices for Software Development

## Which point(s) were new to you?

- Some companies use tools like Trello, Jira for documentation. But if your team is not using any such tool, you should write down the requirements and share it with the team. This will help you get immediate feedback. Also, this will serve as a reference for future conversations
- Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page


## Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I need to improve in doing things with 100% involvement.
- I need to do work with 100% focus without dstractions for that I avoid mobile and distractions from others.
- I have to do work when work, play when I play.