# Energy Management

## 1.What are the activities you do that make you relax - Calm quadrant?  
- Listening to Music.
- Go for walk.
- Eating food.
- Talking to closed persons.

## 2.When do you find getting into the Stress quadrant?
- Working on projects with deadlines is more stress.
- Giving a presentaion or reviews that takes me to more stress.
- Health issues.
- Financial problems.

## 3.How do you understand if you are in the Excitement quadrant?
- Increased Energy Levels.
- Confidence.
- Attending events or gatherings with friends or like-minded people.
- Find it easy to concentrate and stay engaged in your activities. 

## 4.Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep enhances memory and learning.
- Insufficient sleep increases the risk of cancer.
- Consistent sleep patterns are crucial for health.
- Technology and lifestyle choices affect sleep quality.
- Prioritizing sleep can improve overall well-being.

## 5.What are some ideas that you can implement to sleep better?
- Go to bed and wake up at the same time every day.
- Keep your bedroom cool, dark, and quiet.
- Engage in physical activity during the day.
- Avoid screens at least an hour before bed.

## 6.Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- Improves Mood.
- Enhances Focus.
- Protects Brain Health.
- Reduces Stress.
- Boosts Memory.

## 7.What are some steps you can take to exercise more?

- Choose exercises that I enjoy.
- Set Realistic Goals.
- Use a Fitness Tracker.




