# Learning Process

## What is the Feynman Technique? Explain in 1 line.

- Feynman Technique is a method to teach concepts to someone and understand where we are lagging and fill that with better understanding until we got understanding about that concepts.

## In this video, what was the most interesting story or idea for you?

 Pomodoro Technique
   - Select a specific task you want to work on.
   - Set a timer for 25 minutes. 
   - Don't distract to things until clock rings.
   - After clock rings stop work.
   - Take rest for 5-10 min.
   - Repeat the process.

## What are active and diffused modes of thinking?

- Active Mode: This is a focused and concentrated thinking where we actively solve tasks.
- Diffused Mode: This is a relaxed state of mind where we can't concentrate on tasks.

## According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Break down the topic.
- Creat a study plan.
- Engage in Active learning.
- Use multiple resources to learn.
- Teach topics to someone.
- Review your teaching and improve in that concepts.

## What are some of the actions you can take going forward to improve your learning process?

- I have to spend more time on practicing.
- I don't streach mind and give some time to understand the concept.
- I implement Pomodoro Technique in my learning process.



