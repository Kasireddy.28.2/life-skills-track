# Prevention Of Sextual Harashment


## What kinds of behaviour cause sexual harassment?

- Sexual harassment encompasses a wide range of behaviors that are unwelcome and sexual in nature. It can occur in various settings, including workplaces, schools, public spaces, and online.

Some Sextual Harassments are:

- Verbal Harassment
- Physical Harassment
- Written Harassment
- Cyber Harassment

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

 If you face or witness incidents of sexual harassment, it's important to take action to protect yourself and others. Here are steps you can take:

 - If you are in immediate danger, remove yourself from the situation as quickly as possible and find a safe place.
 - Call emergency services if necessary.
 - Talk to someone you trust, such as a friend, family member, or colleague.



