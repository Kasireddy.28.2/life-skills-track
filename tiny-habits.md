# TINY HABITS

## 1.In this video, what was the most interesting story or idea for you?
The concept of "Tiny Habits" by BJ Fogg was intriguing, emphasizing the power of small actions for behavior change. The anecdote about the Maui Habit highlighted the effectiveness of starting with tiny, manageable steps to build lasting habits.

## 2.How can you use B = MAP to make making new habits easier? What are M, A, and P?
BJ Fogg's B = MAP formula suggests that for a behavior to occur, the Motivation (M) to do the behavior, the Ability (A) to perform the behavior, and a Prompt (P) to trigger the behavior must align. Simplifying and optimizing these elements can make forming new habits easier.

## 3.Why is it important to "Shine" or celebrate after each successful completion of a habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
Celebrating after each successful habit completion reinforces positive behavior through positive emotions, making the behavior more likely to become a habit.

## 4.In this video, what was the most interesting story or idea for you?
The idea of "1% Better Every Day" was captivating, emphasizing continuous improvement as a gradual and sustainable approach to personal growth.This idea underscores the significance of consistent small adjustments leading to significant achievements in the long term.

## 5.What is the book's perspective about Identity?
It emphasizes integrating habits into one's identity rather than solely focusing on goal attainment, emphasizing the cumulative impact of small changes to create a positive self-image around habits.

## 6.Write about the book's perspective on how to make a habit easier to do?
The book suggests making habits easier by focusing on small, consistent improvements, optimizing the environment, and employing the two-minute rule to break down tasks into manageable chunks.

## 7.Write about the book's perspective on how to make a habit harder to do?
"Atomic Habits" doesn't delve extensively into making habits harder. Instead, it focuses on arranging environments to facilitate positive habits and deter negative ones. For instance, placing unhealthy snacks out of reach to discourage excessive snacking aligns with the book's approach of making good habits easier to perform.

## 8.Pick one habit that you would like to do more of? What are the steps that you can take to make the cue obvious or the habit more attractive or easy and or response satisfying?
For example, if the desired habit is daily walking, making the cue obvious could involve setting alarm. Making it more attractive may involve following health tips, prefer along with friends.

## 9.Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make the cue invisible or the process unattractive or hard or the response unsatisfying?
To avoid sleep less nights, making the cue invisible might involve keeping devices out of sight. Avoid consuming caffeine and large meals close to bedtime, ensuring the bedroom is dark and cool.